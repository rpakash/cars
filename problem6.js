function problem6(inventory) {
  let bmwAndAudi = [];
  if (Array.isArray(inventory)) {
    for (let i = 0; i < inventory.length; i++) {
      if (inventory[i].car_make === "BMW" || inventory[i].car_make === "Audi") {
        bmwAndAudi.push(inventory[i]);
      }
    }
  }
  return bmwAndAudi;
}

module.exports = problem6;
