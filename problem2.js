let problem2 = (inventory) => {
  if (Array.isArray(inventory) && inventory.length > 0) {
    let lastCar = inventory[inventory.length - 1];
    return lastCar;
  }
  return [];
};

module.exports = problem2;
