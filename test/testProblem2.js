const inventory = require("../cars.js");
const problem2 = require("../problem2.js");

const car = problem2(inventory);
console.log(
  Array.isArray(car) ? car : `Last car is a ${car.car_make} ${car.car_model}`
);
