const problem1 = require("../problem1.js");
const inventory = require("../cars.js");

const car = problem1(inventory, 33);

console.log(
  Array.isArray(car)
    ? car
    : `Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`
);
