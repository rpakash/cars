function problem3(inventory) {
  if (Array.isArray(inventory)) {
    inventory.sort((a, b) => a.car_model.localeCompare(b.car_model));
    return inventory;
  }
  return [];
}

module.exports = problem3;
