function problem1(inventory, searchId) {
  if (Array.isArray(inventory) && Number.isInteger(searchId)) {
    for (let i = 0; i < inventory.length; i++) {
      if (inventory[i].id === searchId) {
        return inventory[i];
      }
    }
  }
  return [];
}

module.exports = problem1;
